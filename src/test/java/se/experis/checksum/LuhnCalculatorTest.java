/*
    The codes used for testing in this test class has first been entered at
    https://simplycalc.com/luhn-calculate.php to verify their validity.

    The credit card numbers was generated at
    https://ccardgenerator.com/index.php
 */

package se.experis.checksum;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class LuhnCalculatorTest {

    LuhnCalculator luhnCalculator;

    @BeforeEach
    void setUp() {
        luhnCalculator = new LuhnCalculator();
    }

    @Test
    void throwsExceptionWhenInputIsEmptyString() {
        assertThrows(IllegalArgumentException.class,
                () -> luhnCalculator.getCheckDigit(""));
        assertThrows(IllegalArgumentException.class,
                () -> luhnCalculator.validate(""));
    }

    @Test
    void throwsExceptionWhenInputIsNotEmptyButNotStringOfIntegers() {
        assertThrows(IllegalArgumentException.class,
                () -> luhnCalculator.validate("NotNumbers"));
        assertThrows(IllegalArgumentException.class,
                () -> luhnCalculator.validate("12345-12345"));

        assertThrows(IllegalArgumentException.class,
                () -> luhnCalculator.getCheckDigit("NotNumbers"));
        assertThrows(IllegalArgumentException.class,
                () -> luhnCalculator.getCheckDigit("12345-12345"));
    }

    @Test
    void doesNotThrowExceptionWhenInputIsStringOfIntegers() {
        assertDoesNotThrow(() -> luhnCalculator.validate("0"));
        assertDoesNotThrow(() -> luhnCalculator.validate("4242424242424242"));
        assertDoesNotThrow(() -> luhnCalculator.validate("999"));

        assertDoesNotThrow(() -> luhnCalculator.getCheckDigit("0"));
        assertDoesNotThrow(() -> luhnCalculator.getCheckDigit("4242424242424242"));
        assertDoesNotThrow(() -> luhnCalculator.getCheckDigit("999"));
    }

    @Test
    void getCheckDigitReturnsTheCorrectCheckDigit() {
        assertEquals(2, luhnCalculator.getCheckDigit("424242424242424"));
        assertEquals(2, luhnCalculator.getCheckDigit("99"));
        assertEquals(0, luhnCalculator.getCheckDigit("0"));
    }

    @Test
    void validateReturnsTrueOnNumbersThatPassLuhnCheck() {
        assertTrue(luhnCalculator.validate("4242424242424242"));
        assertTrue(luhnCalculator.validate("992"));
        assertTrue(luhnCalculator.validate("00"));
    }

    @Test
    void validateReturnsFalseOnNumbersThatDoesNotPassLuhnCheck() {
        assertFalse(luhnCalculator.validate("4242424242424244"));
        assertFalse(luhnCalculator.validate("991"));
        assertFalse(luhnCalculator.validate("01"));
    }

    @Test
    void isCreditCardReturnsTrueOnValidCreditCard() {
        assertTrue(luhnCalculator.isCreditCard("5176006050894694"));
        assertTrue(luhnCalculator.isCreditCard("5431214819381596"));
        assertTrue(luhnCalculator.isCreditCard("5231519697688223"));
    }

    @Test
    void isCreditCardReturnsFalseOnIncorrectLength() {
        assertFalse(luhnCalculator.isCreditCard("517600605089469"));
        assertFalse(luhnCalculator.isCreditCard("543121481938159"));
        assertFalse(luhnCalculator.isCreditCard("523151969768822"));
    }

    @Test
    void isCreditCardReturnsFalseOnCorrectLengthButInvalidCode() {
        assertFalse(luhnCalculator.isCreditCard("5176006050894693"));
        assertFalse(luhnCalculator.isCreditCard("5431214819381598"));
        assertFalse(luhnCalculator.isCreditCard("5231519697688222"));
    }

    @Test
    void getCheckDigitFinishesWithin100Milliseconds() {
        assertTimeoutPreemptively(Duration.ofMillis(100),
                () -> luhnCalculator.getCheckDigit("424242424242424"));
    }

}