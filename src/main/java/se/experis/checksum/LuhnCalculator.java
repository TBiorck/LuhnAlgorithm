package se.experis.checksum;

public class LuhnCalculator {


    private int luhnCheck(String code) {
        int length = code.length();
        int parity = length % 2;
        int sum = 0;

        for (int i = length - 1; i >= 0 ; i--) {
            int digit = Character.getNumericValue(code.charAt(i));

            if (i % 2 == parity) {
                digit = digit * 2;
            }
            if (digit > 9) {
                digit = digit - 9;
            }
            sum += digit;
        }
        return (sum % 10);
    }

    // partCode is a number without the check digit. The method then calculates the correct check digit.
    public int getCheckDigit(String partCode) {
        if (isInvalidInput(partCode)) {
            throw new IllegalArgumentException("Error: Input should be a sequence of numbers!");
        }

        int checksum = luhnCheck(partCode + "0");
        if (checksum == 0) {
            return 0;
        }
        else {
            return 10 - checksum;
        }
    }

    // The last number of fullCode is the check digit.
    public boolean validate(String fullCode) {
        if (isInvalidInput(fullCode)) {
            throw new IllegalArgumentException("Error: Input should be a sequence of numbers!");
        }

        return luhnCheck(fullCode) == 0;
    }

    public boolean isCreditCard(String fullCode) {
        int correctCreditCardLength = 16;
        return fullCode.length() == correctCreditCardLength && validate(fullCode);
    }

    private boolean isInvalidInput(String code) {
        return code.isEmpty() || !code.matches("[0-9]+");
    }
}
