package se.experis.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {

    private String userInput;

    public String askForSequenceOfNumbers() {
        System.out.println("Enter a sequence of numbers: ");
        readInput();
        return userInput;
    }

    private void readInput() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            userInput = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void displayUserInput(){
        System.out.println("Input: " + userInput.substring(0, userInput.length() - 1) + " " + getCheckDigit());
    }

    public void displayCheckDigit() {
        System.out.println("Provided: " + getCheckDigit());
    }

    public void displayCalculatedCheckDigit(int checkDigit) {
        System.out.println("Expected: " + checkDigit);
    }

    public void displayInputValidity(boolean isValid) {
        String result;
        if (isValid) {
            result = "Valid";
        }
        else {
            result = "Invalid";
        }
        System.out.println("Checksum: " + result);
    }

    public void displayCreditCardStatus(String code, boolean isCreditCard) {
        System.out.print("Digits: " + code.length());

        if (isCreditCard) {
            System.out.println(" (Credit Card)");
        }
        else {
            System.out.println(" (Not a Credit Card)");
        }
    }

    public String getUserInput() {
        return userInput;
    }

    private char getCheckDigit() {
        return userInput.charAt(userInput.length() - 1);
    }
}
