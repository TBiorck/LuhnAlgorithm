package se.experis;

import se.experis.checksum.LuhnCalculator;
import se.experis.view.Console;

public class Main {

    public static void main(String[] args) {
        Console console = new Console();
        LuhnCalculator luhnCalculator = new LuhnCalculator();

        String numbers = console.askForSequenceOfNumbers();

        console.displayUserInput();
        console.displayCheckDigit();

        String numbersWithoutCheckDigit = numbers.substring(0, numbers.length() - 1);
        console.displayCalculatedCheckDigit(luhnCalculator.getCheckDigit(numbersWithoutCheckDigit));

        console.displayInputValidity(luhnCalculator.validate(numbers));

        console.displayCreditCardStatus(numbers, luhnCalculator.isCreditCard(numbers));
    }
}
